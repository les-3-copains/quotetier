package com.les3copains.quotetier.config;

import com.les3copains.quotetier.entity.Author;
import com.les3copains.quotetier.entity.Citation;
import com.les3copains.quotetier.repository.AuthorRepository;
import com.les3copains.quotetier.repository.CitationRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataLoader {

    @Bean
    public CommandLineRunner loadData(CitationRepository citationRepository, AuthorRepository authorRepository) {
        return (args) -> {

            // Création des auteurs
            Author author1 = new Author("Albert", "Einstein", 79, "Autrichien");
            Author author2 = new Author("Steve", "Jobs", 67, "USA");
            Author author3 = new Author("Vivian", "Greene", 10, "Jsp");

            // Sauvegarde des auteurs
            authorRepository.save(author1);
            authorRepository.save(author2);
            authorRepository.save(author3);


            // Création et attribution des citations
            Citation citation1 = new Citation("La vie est un mystère qu'il faut vivre, et non un problème à résoudre.", true, 100, 20, author1);
            Citation citation2 = new Citation("La seule façon de faire du bon travail est d'aimer ce que vous faites.", true, 150, 10, author2);
            Citation citation3 = new Citation("La vie n'est pas d'attendre que les tempêtes passent, c'est d'apprendre comment danser sous la pluie.", true, 80, 5, author3);

            // Sauvegarde des citations
            citationRepository.save(citation1);
            citationRepository.save(citation2);
            citationRepository.save(citation3);
        };
    }
}