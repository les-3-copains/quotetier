package com.les3copains.quotetier.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Getter @Setter @ToString
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NonNull private Long id;
    @NonNull private String first_name;
    @NonNull private String last_name;
    @NonNull private int age;
    @NonNull private String nationality;

    public Author(@NonNull String first_name, @NonNull String last_name, @NonNull int age, @NonNull String nationality) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.age = age;
        this.nationality = nationality;
    }

    public Author() {}
}