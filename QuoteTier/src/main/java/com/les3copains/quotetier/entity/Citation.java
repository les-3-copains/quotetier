package com.les3copains.quotetier.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Getter @Setter @ToString
@AllArgsConstructor @NoArgsConstructor
public class Citation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NonNull private Long id;
    @NonNull private String text;
    private boolean verified;
    private int likes;
    private int dislikes;
    @ManyToOne
    @JoinColumn(name = "author_id")
    private Author author;

    public Citation(@NonNull String text, boolean verified, int likes, int dislikes, Author author) {
        this.text = text;
        this.verified = verified;
        this.likes = likes;
        this.dislikes = dislikes;
        this.author = author;
    }
}
