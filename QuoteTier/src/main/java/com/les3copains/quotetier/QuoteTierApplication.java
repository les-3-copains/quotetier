package com.les3copains.quotetier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuoteTierApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuoteTierApplication.class, args);
	}

}
