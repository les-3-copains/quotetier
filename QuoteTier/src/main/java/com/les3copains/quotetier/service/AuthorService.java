package com.les3copains.quotetier.service;


import com.les3copains.quotetier.entity.Author;
import com.les3copains.quotetier.entity.Citation;
import com.les3copains.quotetier.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AuthorService {
    @Autowired
    private AuthorRepository authorRepository;

    public List<Author> getAllAuthors() {
        return authorRepository.findAll();
    }

    public Author getAuthorById(Long id) {
        Optional<Author> author = authorRepository.findById(id);
        return author.orElse(null);
    }

    public Author addAuthor(Author author) {
        return authorRepository.save(author);
    }

    public Author updateAuthor(Long id, Author updatedAuthor) {
        return authorRepository.findById(id)
                .map(author -> {
                    author.setFirst_name(updatedAuthor.getFirst_name());
                    author.setLast_name(updatedAuthor.getLast_name());
                    author.setAge(updatedAuthor.getAge());
                    author.setNationality(updatedAuthor.getNationality());
                    return authorRepository.save(author);
                })
                .orElse(null); // or throw an exception if not found
    }

    public void deleteAuthor(Long id) {
        authorRepository.deleteById(id);
    }
}