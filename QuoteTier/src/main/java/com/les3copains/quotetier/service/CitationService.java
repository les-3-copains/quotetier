package com.les3copains.quotetier.service;

import com.les3copains.quotetier.entity.Author;
import com.les3copains.quotetier.entity.Citation;
import com.les3copains.quotetier.exception.ResourceNotFoundException;
import com.les3copains.quotetier.repository.CitationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@CacheConfig(cacheNames = {"citations"})
public class CitationService {
    @Autowired
    private CitationRepository citationRepository;

    @Cacheable
    public List<Citation> getAllCitations() {
        return citationRepository.findAll();
    }

    public Optional<Citation> getCitation(Long id) { return citationRepository.findById(id);}

    @CacheEvict(allEntries = true)
    public Citation addCitation(Citation citation) {
        return citationRepository.save(citation);
    }

    @CacheEvict(allEntries = true)
    public Citation updateCitation(Long id, Citation citationDetails) {
        Citation citation = citationRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Citation not found"));
        citation.setText(citationDetails.getText());
        citation.setVerified(citationDetails.isVerified());
        citation.setLikes(citationDetails.getLikes());
        citation.setDislikes(citationDetails.getDislikes());
        return citationRepository.save(citation);
    }

    @CacheEvict(allEntries = true)
    public void deleteCitation(Long id) {
        citationRepository.deleteById(id);
    }

    @CacheEvict(allEntries = true)
    public Citation likeCitation(Long id) {
        Citation citation = citationRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Citation not found"));
        citation.setLikes(citation.getLikes() + 1);
        return citationRepository.save(citation);
    }

    @CacheEvict(allEntries = true)
    public Citation dislikeCitation(Long id) {
        Citation citation = citationRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Citation not found"));
        citation.setDislikes(citation.getDislikes() + 1);
        return citationRepository.save(citation);
    }
}
