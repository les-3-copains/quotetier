package com.les3copains.quotetier.repository;

import com.les3copains.quotetier.entity.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author, Long> {
}
