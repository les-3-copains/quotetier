package com.les3copains.quotetier.repository;

import com.les3copains.quotetier.entity.Author;
import com.les3copains.quotetier.entity.Citation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CitationRepository extends JpaRepository<Citation, Long> {
}