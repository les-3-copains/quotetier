package com.les3copains.quotetier.controller;

import com.les3copains.quotetier.entity.Citation;
import com.les3copains.quotetier.service.CitationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/citations")
public class CitationController {
    @Autowired
    private CitationService citationService;

    @GetMapping
    public List<Citation> getAllCitations() {
        return citationService.getAllCitations();
    }

    @GetMapping("/{id}")
    public Optional<Citation> getCitation(@PathVariable Long id) {
        return citationService.getCitation(id);
    }

    @PostMapping
    public Citation createCitation(@RequestBody Citation citation) {
        return citationService.addCitation(citation);
    }

    @PutMapping("/{id}")
    public Citation updateCitation(@PathVariable Long id, @RequestBody Citation citationDetails) {
        return citationService.updateCitation(id, citationDetails);
    }

    @DeleteMapping("/{id}")
    public void deleteCitation(@PathVariable Long id) {
        citationService.deleteCitation(id);
    }

    @PostMapping("/{id}/like")
    public Citation likeCitation(@PathVariable Long id) {
        return citationService.likeCitation(id);
    }

    @PostMapping("/{id}/dislike")
    public Citation dislikeCitation(@PathVariable Long id) {
        return citationService.dislikeCitation(id);
    }
}
