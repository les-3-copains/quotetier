import { Component } from '@angular/core';
import {CitationCardComponent} from "../citation-card/citation-card.component";
import {NgForOf} from "@angular/common";
import {Citation} from "../entities/citation";
import {CitationService} from "../services/citation.service";
import {Router, RouterLink} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {MatToolbar} from "@angular/material/toolbar";
import {MatButton} from "@angular/material/button";
import {MatGridList, MatGridTile} from "@angular/material/grid-list";
import {MatFormField} from "@angular/material/form-field";
import {MatInput} from "@angular/material/input";

@Component({
  selector: 'app-citation-list',
  standalone: true,
  imports: [
    CitationCardComponent,
    NgForOf,
    MatToolbar,
    MatButton,
    RouterLink,
    MatGridList,
    MatGridTile,
    MatFormField,
    MatInput,
    FormsModule
  ],
  templateUrl: './citation-list.component.html',
  styleUrl: './citation-list.component.css'
})
export class CitationListComponent {
  citations: Citation[] = [];
  filteredCitations: Citation[] = [];
  sortByPopularity = false;
  searchText: string = '';

  constructor(private citationService: CitationService, protected router: Router) { }

  ngOnInit(): void {
    this.loadCitations();
  }

  loadCitations(): void {
    this.citationService.getCitations().subscribe(data => {
      this.citations = data;
      this.applySearchFilter(); // Appliquer le filtre de recherche après avoir chargé les citations
    });
  }

  // Fonction pour filtrer les citations en fonction de la recherche
  applySearchFilter(): void {
    if (!this.searchText) {
      this.filteredCitations = this.citations.slice(); // Réinitialiser le filtre si le champ de recherche est vide
      return;
    }
    const searchTextLower = this.searchText.toLowerCase();
    this.filteredCitations = this.citations.filter(citation =>
      citation.text.toLowerCase().includes(searchTextLower)
    );
  }

  likeCitation(id: number): void {
    this.citationService.likeCitation(id).subscribe(() => {
      this.loadCitations();
    });
  }

  dislikeCitation(id: number): void {
    this.citationService.dislikeCitation(id).subscribe(() => {
      this.loadCitations();
    });
  }

  editCitation(id: number): void {
    this.router.navigate([`../citations/edit/${id}`]);
  }

  deleteCitation(id: number): void {
    this.citationService.deleteCitation(id).subscribe(() => {
      this.loadCitations();
    });
  }

  toggleSortByPopularity(): void {
    this.sortByPopularity = !this.sortByPopularity;
    if (this.sortByPopularity) {
      this.filteredCitations.sort((a, b) => (b.likes - b.dislikes) - (a.likes - a.dislikes));
    } else {
      this.filteredCitations = this.citations.slice();
    }
  }
}
