import {Component, EventEmitter, Input, Output} from '@angular/core';
import {MatCard, MatCardActions, MatCardContent} from "@angular/material/card";
import {Citation} from "../entities/citation";
import {MatButton, MatIconButton} from "@angular/material/button";
import {MatIcon} from "@angular/material/icon";
import {
  MatExpansionModule,
  MatExpansionPanel, MatExpansionPanelContent,
  MatExpansionPanelDescription, MatExpansionPanelHeader,
  MatExpansionPanelTitle
} from "@angular/material/expansion";
import {NgIf} from "@angular/common";
import {MatLabel} from "@angular/material/form-field";

@Component({
  selector: 'app-citation-card',
  standalone: true,
  imports: [
    MatCard,
    MatCardContent,
    MatCardActions,
    MatButton,
    MatIcon,
    MatIconButton,
    MatExpansionModule,
    MatExpansionPanel,
    MatExpansionPanelTitle,
    MatExpansionPanelDescription,
    MatExpansionPanelHeader,
    MatExpansionPanelContent,
    NgIf,
    MatLabel
  ],
  templateUrl: './citation-card.component.html',
  styleUrl: './citation-card.component.css'
})
export class CitationCardComponent {
  @Input() citation!: Citation;
  @Output() like = new EventEmitter<number>();
  @Output() dislike = new EventEmitter<number>();
  @Output() edit = new EventEmitter<number>();
  @Output() delete = new EventEmitter<number>();

  likeCitation(): void {
    this.like.emit(this.citation.id);
  }

  dislikeCitation(): void {
    this.dislike.emit(this.citation.id);
  }

  editCitation(): void {
    this.edit.emit(this.citation.id);
  }

  deleteCitation(): void {
    this.delete.emit(this.citation.id);
  }
}
