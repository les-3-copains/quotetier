import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {CitationService} from "../services/citation.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MatCard} from "@angular/material/card";
import {MatFormField, MatLabel} from "@angular/material/form-field";
import {MatCheckbox} from "@angular/material/checkbox";
import {MatInput} from "@angular/material/input";
import {MatButton, MatIconButton} from "@angular/material/button";
import {MatOption, MatSelect} from "@angular/material/select";
import {NgForOf} from "@angular/common";
import {Author} from "../entities/author";
import {AuthorService} from "../services/author.service";
import {MatIcon} from "@angular/material/icon";


@Component({
  selector: 'app-citation-form',
  standalone: true,
  imports: [
    MatCard,
    ReactiveFormsModule,
    MatFormField,
    MatCheckbox,
    MatInput,
    MatButton,
    MatLabel,
    MatSelect,
    MatOption,
    NgForOf,
    MatIcon,
    MatIconButton,
  ],
  templateUrl: './citation-form.component.html',
  styleUrl: './citation-form.component.css'
})
export class CitationFormComponent implements OnInit{
  citationForm: FormGroup;
  citationId: number | undefined;
  authors: Author[] = [];

  constructor(
    private fb: FormBuilder,
    private citationService: CitationService,
    private authorService: AuthorService,
    private route: ActivatedRoute,
    protected router: Router
  ) {
    this.citationForm = this.fb.group({
      text: ['', Validators.required],
      author: ['', Validators.required],
      verified: [false],
      likes: [0],
      dislikes: [0]
    });
  }

  ngOnInit(): void {
    this.citationId = this.route.snapshot.params['id'];
    if (this.citationId) {
      this.citationService.getCitation(this.citationId).subscribe(data => {
        this.citationForm.patchValue(data);
        console.log("onInit : ",  this.citationForm);
      });
    }
    this.loadAuthors();
  }

  loadAuthors(): void {
    this.authorService.getAllAuthors().subscribe(authors => {
      this.authors = authors;
    });
  }

  onSubmit(): void {
    console.log("Citation :", this.citationForm.value);
    if (this.citationId) {
      this.citationService.updateCitation(this.citationId, this.citationForm.value).subscribe(() => {
        console.log("update Citation :", this.citationForm.value);
        this.router.navigate(['/citations']);
      });
    } else {
      this.citationService.addCitation(this.citationForm.value).subscribe(() => {
        console.log("add Citation :", this.citationForm.value);
        this.router.navigate(['/citations']);
      });
    }
  }
}
