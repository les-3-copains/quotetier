import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CitationFormComponent } from './citation-form.component';

describe('CitationFormComponent', () => {
  let component: CitationFormComponent;
  let fixture: ComponentFixture<CitationFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CitationFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CitationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
