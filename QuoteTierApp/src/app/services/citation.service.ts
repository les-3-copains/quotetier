import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Citation} from "../entities/citation";

@Injectable({
  providedIn: 'root'
})
export class CitationService {
  private apiUrl = 'http://localhost:8080/api/citations';

  constructor(private http: HttpClient) { }

  getCitations(): Observable<Citation[]> {
    return this.http.get<Citation[]>(this.apiUrl);
  }

  getCitation(id: number): Observable<Citation> {
    return this.http.get<Citation>(`${this.apiUrl}/${id}`);
  }

  addCitation(citation: Citation): Observable<Citation> {
    return this.http.post<Citation>(this.apiUrl, citation);
  }

  updateCitation(id: number, citation: Citation): Observable<Citation> {
    return this.http.put<Citation>(`${this.apiUrl}/${id}`, citation);
  }

  deleteCitation(id: number): Observable<void> {
    return this.http.delete<void>(`${this.apiUrl}/${id}`);
  }

  likeCitation(id: number): Observable<Citation> {
    return this.http.post<Citation>(`${this.apiUrl}/${id}/like`, {});
  }

  dislikeCitation(id: number): Observable<Citation> {
    return this.http.post<Citation>(`${this.apiUrl}/${id}/dislike`, {});
  }
}
