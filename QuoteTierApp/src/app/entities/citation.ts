import { Author } from './author';

export class Citation {
  id: number;
  text: string;
  verified: boolean;
  likes: number;
  dislikes: number;
  author: Author;

  constructor(id: number, text: string, verified: boolean, likes: number, dislikes: number, author: Author) {
    this.id = id;
    this.text = text;
    this.verified = verified;
    this.likes = likes;
    this.dislikes = dislikes;
    this.author = author;
  }
}
