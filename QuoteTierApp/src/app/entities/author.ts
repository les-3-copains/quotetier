export class Author {
  id: number;
  first_name: string;
  last_name: string;
  age: number;
  nationality: string;

  constructor(id: number, first_name: string, last_name: string, age: number, nationality: string) {
    this.id = id;
    this.first_name = first_name;
    this.last_name = last_name;
    this.age = age;
    this.nationality = nationality;
  }
}
