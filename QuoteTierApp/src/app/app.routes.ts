import { Routes } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {CitationFormComponent} from "./citation-form/citation-form.component";
import {CitationListComponent} from "./citation-list/citation-list.component";
import {AuthorFormComponent} from "./author-form/author-form.component";

export const routes: Routes = [
  { path: '', title: 'Add Citation',component: HomeComponent },
  { path: 'citations/add', component: CitationFormComponent },
  { path: 'citations/edit/:id', component: CitationFormComponent },
  { path: 'citations', component: CitationListComponent },
  { path: 'authors/add', component: AuthorFormComponent },
  { path: 'authors/edit/:id', component: AuthorFormComponent },
];
