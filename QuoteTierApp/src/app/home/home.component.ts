import { Component } from '@angular/core';
import {MatCardModule} from "@angular/material/card";
import {MatIconModule} from "@angular/material/icon";
import {ToolbarComponent} from "../toolbar/toolbar.component";
import {MatButton} from "@angular/material/button";
import {RouterLink} from "@angular/router";
import {CitationFormComponent} from "../citation-form/citation-form.component";

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [MatCardModule, MatIconModule, ToolbarComponent, MatButton, RouterLink, CitationFormComponent],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent {

}
