import { Component } from '@angular/core';
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {AuthorService} from "../services/author.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MatCard} from "@angular/material/card";
import {MatFormField, MatLabel} from "@angular/material/form-field";
import {MatInput} from "@angular/material/input";
import {MatButton} from "@angular/material/button";

@Component({
  selector: 'app-author-form',
  standalone: true,
  imports: [
    MatCard,
    ReactiveFormsModule,
    MatFormField,
    MatInput,
    MatButton,
    MatLabel,
  ],
  templateUrl: './author-form.component.html',
  styleUrl: './author-form.component.css'
})
export class AuthorFormComponent {
  authorForm: FormGroup;
  authorId: number | undefined;
  previousUrl: string | null = null;

  constructor(
    private fb: FormBuilder,
    private authorService: AuthorService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.authorForm = this.fb.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      age: [null, Validators.required],
      nationality: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.authorId = this.route.snapshot.params['id'];
    if (this.authorId) {
      this.authorService.getAuthor(this.authorId).subscribe(data => {
        this.authorForm.patchValue(data);
      });
    }
    this.route.queryParams.subscribe(params => {
      this.previousUrl = params['returnUrl'];
    });
  }

  onSubmit(): void {
    if (this.authorId) {
      this.authorService.updateAuthor(this.authorId, this.authorForm.value).subscribe(() => {
        this.navigateBack();
      });
    } else {
      this.authorService.addAuthor(this.authorForm.value).subscribe(() => {
        this.navigateBack();
      });
    }
  }

  onCancel(): void {
    this.navigateBack();
  }

  private navigateBack(): void {
    if (this.previousUrl) {
      this.router.navigateByUrl(this.previousUrl);
    } else {
      this.router.navigate(['/citations']);
    }
  }
}
